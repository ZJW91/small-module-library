const aes = require('../../utils/aes.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inputValue:'hPUpWboG4mimbDFNqYH20V4huxlJsjjJF1ahM6jNps14YSW0cOYaWPTUoWlJQ8hOpZytaJbapceG6iRZKV3KTUG5zhXSpX9DeFCjDA0LZXr4/3pfHBfaQ8wDdYX2RxaGpP/GZUkgR0o8TCQlkQLduyUvzXZid6D7px5NJmS2MZv8NESyFQtkZs/E0uYRmEly0y91qeoXiBJYL2HW4rMpWQ3a955WBuBNzwbHzOKjP8IvioBK2hy3BQrXzt1Yd8NW',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {

  },


  /**
   * 输入监听
   */
  inputListen(e){
    const that = this;
    that.setData({
      inputValue:e.detail.value
    })
  },
  
  /**
   * 加密
   */
  setCode(){
    const that = this;
    that.setData({
      inputValue:aes.Encrypt(that.data.inputValue)// 加密,
    })
  },
  /**
   * 解密
   */
  getValue(){
    const that = this;
    let str  =aes.Decrtpt(that.data.inputValue)
    console.log(str)
    that.setData({
      inputValue:aes.Decrtpt(that.data.inputValue)//解密,
    })
  },
  /**
   * 清空
   */
  clear(){
    this.setData({
      inputValue:'',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})