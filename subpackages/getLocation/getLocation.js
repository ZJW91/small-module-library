// pages/getLocation/getLocation.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 微信经纬度转百度地址
    wxLocation:'',
    bdLocaiton:'',
    bdAddress:'',

    // 百度经纬度转百度地址
    bdLocaiton2:'',
    bdAddress2:'',

    // 高德经纬度转高德地址
    gdLocaiton:'',
    gdAddress:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 方式1、
   * 微信经纬度--->百度地址
   */
  wxLocaitonToBdAddress() {
    const that = this
    wx.showLoading({
      title:'位置获取中...',
      mask:true
    })
    wx.authorize({
      scope: 'scope.userLocation',
      success(res) {
        console.log(res,999)
        if (res.errMsg == 'authorize:ok') {
          wx.getLocation({
            type: 'gcj02', // 高精度坐标
            isHighAccuracy: true, // 开启高精度
            success: function (res) {
                // console.log('getLocation:success', res)
                var latitude = res.latitude
                var longitude = res.longitude
                that.setData({
                  wxLocation:longitude +'-'+ latitude,
                })
                console.log('微信经纬度:',longitude,latitude);
                that.wxLocationToBdAdress(latitude, longitude); //百度逆地理解析
            },
            fail: function (res) {
              console.log('fail', res);
            },
          });
        }
      },
      fail(err) {
        console.log(err,11111)
      }
    })
  },
  wxLocationToBdAdress(latitude, longitude) {
    const bmap = require('../../utils/bmap-wx.min.js');
    const BMap = new bmap.BMapWX({
      ak: 'b5xnRWivaiEXs9xKP1FwpGeCccvnmnfR'
    });
    BMap.regeocoding({
      location: `${latitude},${longitude}`,
      coord_type: 'gcj02', // 指定输入坐标的坐标系,因为微信是gcj02的
      success: (res) => {
        wx.hideLoading()
        console.log('百度位置信息:', res);
        let longitude = res.originalData.result.location.lng
        let latitude = res.originalData.result.location.lat
        console.log("百度经纬度：",longitude+','+latitude)
        const address = res.originalData.result.formatted_address_poi;
        this.setData({
          bdLocaiton: longitude + ',' + latitude,
          bdAddress: address
        });
      },
      fail: (err) => {
        console.error('百度地图逆地理编码失败', err);
      }
    });
  },


  /**
   * 方式2、
   * 百度经纬度--->百度地址
   */
  bdLocationToBdAddress() {
    const that = this;
    wx.showLoading({
      title: '位置获取中...',
      mask: true,
    });
    const bmap = require('../../utils/bmap-wx.min.js');
    const BMap = new bmap.BMapWX({
      ak: 'b5xnRWivaiEXs9xKP1FwpGeCccvnmnfR'  // 使用你的百度 API Key
    });
    BMap.regeocoding({
      fail: function (err) {
        console.log('百度地图定位失败', err);
        wx.hideLoading();
      },
      success: function (res) {
        console.log('百度位置信息2: ', res);
        wx.hideLoading();
        let longitude = res.originalData.result.location.lng;
        let latitude = res.originalData.result.location.lat;
        let address = res.originalData.result.formatted_address_poi;
        console.log("百度经纬度2：",longitude+','+latitude)
        that.setData({
          bdLocaiton2:longitude+','+latitude,
          bdAddress2: address,
        });
      },
    });
  }, 



  /**
   * 方式4、
   * 高德经纬度->高德地址
   */
  gdLocationToGdAddress() {
  const amapFile = require('../../utils/amap-wx.130.js');
    const that = this;
    wx.showLoading({
      title: '位置获取中...',
      mask: true,
    });
    const myAmapFun = new amapFile.AMapWX({
      key: 'aacdf41235767a5914e32aa899a1d580', // 使用你的高德 API Key
    });
    // 调用高德地图的getRegeo方法获取位置信息
    myAmapFun.getRegeo({
      success: function (res) {
        console.log('高德位置信息:', res);
        wx.hideLoading();
        if (res && res[0]) {
          const address = res[0].regeocodeData.formatted_address;
          const longitude = res[0].longitude;
          const latitude = res[0].latitude;
          // 设置页面的data
          that.setData({
            gdAddress: address,
            gdLocaiton: longitude+','+latitude,
          });
        }
      },
      fail: function (err) {
        console.log('高德地图定位失败', err);
        wx.hideLoading();
      },
    });
  },  



  /**
   * 手动将微信经纬度转成百度经纬度
   * @param {*} lng 
   * @param {*} lat 
   */
  gcj02ToBd09(lng, lat) {
    const x_pi = (Math.PI * 3000.0) / 180.0;
    const z = Math.sqrt(lng * lng + lat * lat) + 0.00002 * Math.sin(lat * x_pi);
    const theta = Math.atan2(lat, lng) + 0.000003 * Math.cos(lng * x_pi);
    const bd_lng = z * Math.cos(theta) + 0.0065;
    const bd_lat = z * Math.sin(theta) + 0.006;
    console.log("腾讯经纬度转百度经纬度：",bd_lng,bd_lat)
    return {
      lng: bd_lng,
      lat: bd_lat
    };
  },



/**
 * 微信信息授权----太繁琐暂不用
 */
// onAuthLocation: function () {
//     let that = this
//     wx.getSetting({
//         success: (res) => {
//             // res.authSetting有多种属性，比如res.authSetting['scope.userInfo']表示获取用户头像等信息，详情看官方文档
//             // res.authSetting['scope.userLocation'] == undefined    表示 初始化进入该页面
//             // res.authSetting['scope.userLocation'] == false    表示 非初始化进入该页面,且未授权
//             // res.authSetting['scope.userLocation'] == true    表示 地理位置授权
//             // 拒绝授权后再次进入重新授权
//             if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
//                 // console.log('authSetting:status:拒绝授权后再次进入重新授权', res.authSetting['scope.userLocation'])
//                 wx.showModal({
//                     title: '',
//                     content: '【xxx】需要获取你的地理位置，请确认授权',
//                     success: function (res) {
//                         if (res.cancel) {
//                             wx.showToast({
//                                 title: '拒绝授权',
//                                 icon: 'none'
//                             })
//                         } 
//                         else if (res.confirm) {
//                             wx.openSetting({
//                                 success: function (dataAu) {
//                                     // console.log('dataAu:success', dataAu)
//                                     if (dataAu.authSetting["scope.userLocation"] == true) {
//                                         //再次授权，调用wx.getLocation的API
//                                         that.wxLocaitonToBdAddress(dataAu)
//                                     } else {
//                                         wx.showToast({
//                                             title: '授权失败',
//                                             icon: 'none'
//                                         })
//                                     }
//                                 }
//                             })
//                         }
//                     }//success
//                 })
//             }
//             // 初始化进入，未授权
//             else if (res.authSetting['scope.userLocation'] == undefined) {
//                 // console.log('authSetting:status:初始化进入，未授权', res.authSetting['scope.userLocation'])
//                 //调用wx.getLocation的API
//                 that.wxLocaitonToBdAddress(res)
//             }
//             // 已授权
//             else if (res.authSetting['scope.userLocation']) {
//                 // console.log('authSetting:status:已授权', res.authSetting['scope.userLocation'])
//                 //调用wx.getLocation的API
//                 that.wxLocaitonToBdAddress(res)
//             }
//         }
//     })
// },
// wxLocaitonToBdAddress: function (userLocation) {
//     let that = this
//     wx.getLocation({
//         type: 'gcj02', // 高精度坐标
//         isHighAccuracy: true, // 开启高精度
//         success: function (res) {
//             // console.log('getLocation:success', res)
//             var latitude = res.latitude
//             var longitude = res.longitude
//             that.setData({
//               wxLocation:longitude +'-'+ latitude,
//             })
//             console.log('微信经纬度:',longitude,latitude);
//             that.wxLocationToBdAdress(latitude, longitude); // 使用百度 API 进行逆地理编码
//         },
//         fail: function (res) {
//             console.log('fail', res.errMsg);
//             if (res.errMsg === 'getLocation:fail:auth denied') {
//                 wx.showToast({
//                     title: '拒绝授权',
//                     icon: 'none'
//                 })
//                 return
//             }
//             if (!userLocation || !userLocation.authSetting['scope.userLocation']) {

//             } 
//             else if (userLocation.authSetting['scope.userLocation']) {
//                 wx.showModal({
//                     title: '',
//                     content: '请在系统设置中打开定位服务',
//                     showCancel: false,
//                     success: result => {
//                         if (result.confirm) {
//                             wx.navigateBack()
//                         }
//                     }
//                 })
//             }
//             else {
//                 wx.showToast({
//                     title: '授权失败',
//                     icon: 'none'
//                 })
//             }
//         }
//     })
// },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})