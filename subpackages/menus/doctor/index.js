Page({
  data: {
    currentTab: 0,
    tabs: [
      {
        name: '全部',
        doctors: [
          { name: '李芳医生', title: '主任医师 医学博士', hospital: '同济大学附属东方医院', image: 'https://covidsh.dkmksh.com/PDF/static/yulong/yl2/yljk/doctor1.png' },
          { name: '李医生', title: '妇科主任', hospital: '同济大学附属东方医院', image: 'https://covidsh.dkmksh.com/PDF/static/yulong/yl2/yljk/doctor2.png' }
        ]
      },
      {
        name: '妇科',
        doctors: [
          { name: '李芳医生', title: '主任医师 医学博士', hospital: '同济大学附属东方医院', image: 'https://covidsh.dkmksh.com/PDF/static/yulong/yl2/yljk/doctor1.png' }
        ]
      },
      {
        name: '乳腺科',
        doctors: [ { name: '李芳医生', title: '主任医师 医学博士', hospital: '同济大学附属东方医院', image: 'https://covidsh.dkmksh.com/PDF/static/yulong/yl2/yljk/doctor1.png' }]
      },
      {
        name: '妇产科',
        doctors: [ { name: '李医生', title: '妇科主任', hospital: '同济大学附属东方医院', image: 'https://covidsh.dkmksh.com/PDF/static/yulong/yl2/yljk/doctor2.png' }]
      }
    ]
  },

  switchTab(e) {
    const index = e.currentTarget.dataset.index;
    this.setData({
      currentTab: index
    });
  }
});
