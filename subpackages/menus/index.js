// pages/demo/demo.js
Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    // tab选项
    tabParams:{
      chooseCurrentParentIndex: 0,  //一级导航index
      chooseCurrentSecondIndex: 0, //二级导航index
      chooseCurrentTabFirst: 1, //默认一级菜单激活位置
      chooseCurrentTabSecond: 1, //默认二级菜单激活位置
    },
    //菜单数据
    tabList:[
      {'title':'一级名称1',sort:'1',id:"一级id1",childList:[
        {'title':'二级tab1',sort:'1',id:"111-1",},
        {'title':'二级tab2',sort:'2',id:"111-2",},
      ]},
      {'title':'一级名称2',sort:'2',id:"一级id2",childList:[
        {'title':'二级tab1',sort:'1',id:"222-1",},
        {'title':'二级tab2',sort:'2',id:"222-2",},
        {'title':'二级tab3',sort:'3',id:"222-3",}
      ]}
    ],//tab导航

    //一级菜单
    tabId:"",
    tabName:"",
    //二级菜单
    parentTabId:"",
    parentTabName:"",
  },
 
  // 点击tab切换更新请求
  changeTab(e){
    let that = this;
    console.log(e)
    let params = e.detail;//获取点击tab后获取导航（一级和二级id）
    let tabType = params.tabType;
    let chooseCurrentParentIndex = params.chooseCurrentParentIndex;//一级index
    let chooseCurrentSecondIndex = params.chooseCurrentSecondIndex;//二级index
    let parentTabData = that.data.tabList[chooseCurrentParentIndex]

    if(tabType == '1'){//一级导航
        that.setData({
          parentTabId:parentTabData.id,
          parentTabName:parentTabData.title,
          tabId:parentTabData.childList[0].id,
          tabName:parentTabData.childList[0].title,
        })
        console.log(that.data.parentTabId,that.data.parentTabName,that.data.tabId,that.data.tabName)
    }else{
        that.setData({
          parentTabId:parentTabData.id,
          parentTabName:parentTabData.title,
          tabId:parentTabData.childList[chooseCurrentSecondIndex].id,
          tabName:parentTabData.childList[chooseCurrentSecondIndex].title,
        })
        console.log(that.data.parentTabId,that.data.parentTabName,that.data.tabId,that.data.tabName)

    }
  },
})