import {
  consultation
} from "../../../api/api"
Page({
  data: {
    currentTab: '', // 默认选中第一个
    tabs: {
      "CT": [
        {
          "diseaseName": "* 肺部CT：肺小结节",
          "hexamName": "CT",
          "id": 1,
          "keyword": "肺小结节1",
          "active": false
        },
        {
          "diseaseName": "肺小结节",
          "hexamName": "CT",
          "id": 2,
          "keyword": "肺小结节2",
          "active": false
        }
      ],
      "X光摄片": [
        {
          "diseaseName": "右侧肾区高密度影  右肾结石待排",
          "hexamName": "X光摄片",
          "id": 88,
          "keyword": "右侧肾区高密度影",
          "active": false
        },
        {
          "diseaseName": "右下肺钙化点",
          "hexamName": "X光摄片",
          "id": 89,
          "keyword": "右下肺钙化点",
          "active": false
        }
      ],
    },
    selectedKeywords: [],  // 用于存储选中的标签的keyword
    menuTabs: []  // 用于存储菜单的数组
  },
  onLoad: function() {  
    var that = this

    //后台获取数据
    // consultation({
    // }).then(res => {
    //   if(res.result){
    //     that.dataHandle(res.result)
    //   }
    // })

    that.dataHandle(that.data.tabs)//写死数据

  },

  // 处理数据
  dataHandle(list){
      var that = this;
          // 将 tabs 对象转换为数组
    const menuTabs = Object.keys(list);
    const tabs = list;
    
    // 处理 tags 数据，添加 displayName 字段
    menuTabs.forEach(tab => {
      tabs[tab].forEach(tag => {
        tag.displayName = tag.keyword || tag.diseaseName;
      });
    });

    // 设置第一个 tab 为默认选中
    const defaultTab = menuTabs[0];
    this.setData({ 
      currentTab: defaultTab,
      menuTabs,
      tabs
    });
  },

  switchTab: function(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.tabName
    });
  },

  toggleTag: function(e) {
    let that = this;
    const tagId = e.currentTarget.dataset.id;
    const currentTab = this.data.currentTab;
    const tabs = this.data.tabs;
    const tags = tabs[currentTab];
    const selectedTags = tags.filter(tag => tag.active);
    const index = tags.findIndex(tag => tag.id === tagId);
    if (index !== -1) {
        const keyword = tags[index].keyword || tags[index].diseaseName; // 如果keyword没有值就收集diseaseName
        tags[index].active = !tags[index].active;
        this.setData({
            tabs: this.data.tabs
        }, () => {
            const updatedTags = this.data.tabs[currentTab];
            const updatedSelectedTags = updatedTags.filter(tag => tag.active);
            const updatedSelectedCount = updatedSelectedTags.length;

            if (tags[index].active) {
                if (that.data.selectedKeywords.length >= 10) {
                    wx.showModal({
                        title: '提示',
                        content: '最多只能选择10个tag',
                        showCancel: false,
                        confirmText: '知道了'
                    });
                    // 如果已经达到10个，则不再增加此tag
                    tags[index].active = false;
                    this.setData({
                        tabs: this.data.tabs
                    });
                } else {
                    this.setData({
                        selectedKeywords: [...this.data.selectedKeywords, keyword]
                    });
                }
            } else {
                const updatedSelectedKeywords = this.data.selectedKeywords.filter(kw => kw !== keyword);
                this.setData({
                    selectedKeywords: updatedSelectedKeywords
                });
            }
        });
    }
},

  // 点击按钮打印选中tag的keyword
  printSelectedKeywords: function() {
    let that = this;
    let sentence = "";
    const selectedKeywords = that.data.selectedKeywords;
    console.log(selectedKeywords.length)
    if(selectedKeywords.length > 10){
      wx.showModal({
        title: '提示',
        content: '最多只能选择10个',
        showCancel: false,
        confirmText: '知道了'
    });
    }else if(selectedKeywords.length == 0){
      wx.showModal({
        title: '提示',
        content: '请选择标签',
        showCancel: false,
        confirmText: '知道了'
    });
    }else{
      sentence = "我想咨询【" + selectedKeywords.join("】、【") + "】的临床医学解读与健康管理建议"
      console.log(sentence);
    }
  },
});
