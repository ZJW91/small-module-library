// pages/demo/demo.js
Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    // tab选项
    tabParams:{
      chooseCurrent: 0,  //默认一级导航id
      chooseCurrentTabFirst: 1, //当前导航(一级)下标
      chooseCurrentSecond: 0, //默认二级导航id
      chooseCurrentTabSecond: 1, //当前导航(二级)下标
    },
    tabList:[
      {'title':'一级tab1',id:'1',prvId:'',nextId:'2',childList:[
        {'title':'二级tab1',id:'1',prvId:'',nextId:'2',},
        {'title':'二级tab2',id:'2',prvId:'1',nextId:'3',},
      ]},
      {'title':'一级tab2',id:'2',prvId:'1',nextId:'3',childList:[
        {'title':'二级tab1',id:'1',prvId:'',nextId:'2',},
        {'title':'二级tab2',id:'2',prvId:'1',nextId:'3',},
        {'title':'二级tab3',id:'3',prvId:'2',nextId:'4',}
      ]}

    ],//tab导航
    // 模拟滑动页面 切换导航
    openNum:true,
    spedNum:30,//滑动距离
    background: ['demo-text-1'],
    indicatorDots: false,
    vertical: false,
    autoplay: false,
    circular: false,
    interval: 2000,
    duration: 300,
    previousMargin: 0,
    nextMargin: 0,
    TypeLists: [
      {posterId: 10561005, typeDesc: "央视网"},
      {posterId: 10561003, typeDesc: "爱奇艺"},
      {posterId: 10561008, typeDesc: "腾讯视频"},
      {posterId: 10561010, typeDesc: "PPTV"},
      {posterId: 10561002, typeDesc: "优酷"},
      {posterId: 10561007, typeDesc: "哔哩哔哩"}, 
      {posterId: 10561006, typeDesc: "芒果TV"}, 
      {posterId: 10561005, typeDesc: "央视网"},
      {posterId: 10561003, typeDesc: "爱奇艺"},
      {posterId: 10561008, typeDesc: "腾讯视频"},
      {posterId: 10561010, typeDesc: "PPTV"},
      {posterId: 10561002, typeDesc: "优酷"},
      {posterId: 10561007, typeDesc: "哔哩哔哩"}, 
      {posterId: 10561006, typeDesc: "芒果TV"}, 
      {posterId: 10561004, typeDesc: "西瓜视频"}
    ],   //导航栏假数据
  },
 
  // 点击tab切换更新请求
  changeTab(e){
    console.log(e)
    let params = e.detail;//获取点击tab后获取导航（一级和二级id）
    // let type = params.type;//当前点击导航级别  1 代表一级
    // let id = params.id;//当前点击导航id
    // let index = params.index;//当前点击导航 index
    // if(type==1){
      this.setData({
        'tabParams.chooseCurrent':params.chooseCurrent,
        'tabParams.chooseCurrentTabFirst':params.chooseCurrentTabFirst,
        'tabParams.chooseCurrentTabSecond':params.chooseCurrentTabSecond,
        'tabParams.chooseCurrentSecond':params.chooseCurrentSecond,
      })
    // }else{
    //   this.setData({
    //     'tabParams.chooseCurrentTabSecond':id,
    //     'tabParams.chooseCurrentSecond':index
    //   })
    // }
  },
  // 滑动页面切换导航
  
  changeProperty: function (e) {
    console.log('changeProperty')
    // console.log(e)
    var propertyName = e.currentTarget.dataset.propertyName
    var newData = {}
    newData[propertyName] = e.detail.value
    this.setData(newData)
  },
  changeIndicatorDots: function (e) {
    // console.log('changeIndicatorDots')
    // console.log(e)
    // 判断左滑还是右滑
    let currentItem = this.data.tabList[this.data.tabParams.chooseCurrent];
    let currentSecondItem = this.data.tabList[this.data.tabParams.chooseCurrent].childList[this.data.tabParams.chooseCurrentSecond];
    // let openNum = true
    if(e.detail.dx == 0){
      this.setData({
        openNum:true,
      })
    }
    // 左滑
    if(e.detail.dx > 0 && e.detail.dx > this.data.spedNum && this.data.openNum ){
      // openNum = false
    console.log(e.detail.dx)
    console.log(this.data.tabParams.chooseCurrent)
      this.setData({
        openNum:false,
      })
      // 左滑
      // 先判断二级是否是最后一个
      if(this.data.tabParams.chooseCurrentSecond < this.data.tabList[this.data.tabParams.chooseCurrent].childList.length-1){
        this.setData({
          'tabParams.chooseCurrentSecond':this.data.tabParams.chooseCurrentSecond+1,
        })
        this.setData({
          'tabParams.chooseCurrentTabSecond': this.data.tabList[this.data.tabParams.chooseCurrent].childList[this.data.tabParams.chooseCurrentSecond].id,
        })
      }else{
        // 如果二级是最后一个 判断 一级是否是最后一个
        if(this.data.tabParams.chooseCurrent < this.data.tabList.length-1){
          this.setData({
            'tabParams.chooseCurrent':this.data.tabParams.chooseCurrent+1,
          })
          this.setData({
            'tabParams.chooseCurrentTabFirst':this.data.tabList[this.data.tabParams.chooseCurrent].id,
            'tabParams.chooseCurrentTabSecond':this.data.tabList[this.data.tabParams.chooseCurrent].childList[0].id,
            'tabParams.chooseCurrentSecond':0
          })
        }
      }
    }
    // 右滑
    if(e.detail.dx < 0 && e.detail.dx < this.data.spedNum*-1 && this.data.openNum ){
      // openNum = false
    console.log(e.detail.dx)
    console.log(this.data.tabParams.chooseCurrent)
      this.setData({
        openNum:false,
      })
      // 右滑
      // 先判断二级是否是第一个
      if(this.data.tabParams.chooseCurrentSecond !=0){
        this.setData({
          'tabParams.chooseCurrentSecond':this.data.tabParams.chooseCurrentSecond-1,
        })
        this.setData({
          'tabParams.chooseCurrentTabSecond': this.data.tabList[this.data.tabParams.chooseCurrent].childList[this.data.tabParams.chooseCurrentSecond].id,
        })
      }else{
        // 如果二级是第一个 判断 一级是否是第一个
        if(this.data.tabParams.chooseCurrent !=0){
          this.setData({
            'tabParams.chooseCurrent':this.data.tabParams.chooseCurrent-1,
          })
          this.setData({
            'tabParams.chooseCurrentTabFirst':this.data.tabList[this.data.tabParams.chooseCurrent].id,
            'tabParams.chooseCurrentTabSecond':this.data.tabList[this.data.tabParams.chooseCurrent].childList[0].id,
            'tabParams.chooseCurrentSecond':0
          })
        }
      }
    }
    // this.setData({
    //   indicatorDots: !this.data.indicatorDots
    // })
  },
  changeAutoplay: function (e) {
    this.setData({
      autoplay: !this.data.autoplay
    })
  },
  intervalChange: function (e) {
    this.setData({
      interval: e.detail.value
    })
  },
  durationChange: function (e) {
    this.setData({
      duration: e.detail.value
    })
  },
  // end
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
 
  },
 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
 
  },
 
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
 
  },
 
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
 
  },
 
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
 
  },
 
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
 
  },
 
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
 
  },
 
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
 
  }
})