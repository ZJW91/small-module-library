    //test.js
    Page({
      data:{
        qRCodeMsg:''
      },
      getQRCode: function(){
        var that = this;
        wx.scanCode({
        onlyFromCamera:false,//true只能相机扫码
        scanType:['barCode', 'qrCode'],//默认为二维码，barCode条码
          success: function(res){
            console.log(res);    //输出回调信息
            that.setData({
              qRCodeMsg: res.result
            });
            wx.showToast({
              title: '成功',
              duration: 2000
            })
          }
        })
      }
    })