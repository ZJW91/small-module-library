
/**
 * 方法一、通过定时器去查询用户位置
 */
// 引入百度地图 SDK
const bmap = require('../../utils/bmap-wx.min.js');
Page({
  data: {
    latitude: 0,
    longitude: 0,
    address: '',
  },
  onLoad() {
    // 开始实时监听位置变化
    this.startLocationPolling();
  },
  // 定时获取位置
  startLocationPolling() {
    this.locationTimer = setInterval(() => {
      // 获取用户当前位置
      wx.getLocation({
        type: 'gcj02', // 返回可以用于百度地图的 GCJ-02 坐标
        success: (res) => {
          console.log('当前位置：', res);
          this.setData({
            latitude: res.latitude,
            longitude: res.longitude
          });
          // 使用百度 API 进行逆地理编码
          this.reverseGeocode(res.latitude, res.longitude);
        },
        fail: (err) => {
          console.error('获取位置失败', err);
        }
      });
    }, 10000); // 每10秒获取一次位置
  },
  // 使用百度 API 进行逆地理编码
  reverseGeocode(latitude, longitude) {
    const BMap = new bmap.BMapWX({
      ak: 'b5xnRWivaiEXs9xKP1FwpGeCccvnmnfR'
    });
    BMap.regeocoding({
      location: `${latitude},${longitude}`,
      coord_type: 'gcj02', // 指定输入坐标的坐标系
      success: (res) => {
        console.log('百度地图位置信息:', res);
        const address = res.originalData.result.formatted_address_poi;
        this.setData({
          address: address
        });
      },
      fail: (err) => {
        console.error('百度地图逆地理编码失败', err);
      }
    });
  },
  onUnload() {
    // 页面卸载时清除定时器
    if (this.locationTimer) {
      clearInterval(this.locationTimer);
    }
  },
  //手动将微信经纬度转成百度经纬度方法-暂时不用
  gcj02ToBd09(lng, lat) {
    const x_pi = (Math.PI * 3000.0) / 180.0;
    const z = Math.sqrt(lng * lng + lat * lat) + 0.00002 * Math.sin(lat * x_pi);
    const theta = Math.atan2(lat, lng) + 0.000003 * Math.cos(lng * x_pi);
    const bd_lng = z * Math.cos(theta) + 0.0065;
    const bd_lat = z * Math.sin(theta) + 0.006;
    return {
      lng: bd_lng,
      lat: bd_lat
    };
  },
});






/**
 * 方法二、通过微信实时位置移动监听，还需要开通相应的接口，比较麻烦
 */
// // 引入百度地图 SDK
// const bmap = require('../../utils/bmap-wx.min.js');
// Page({
//   data: {
//     latitude: 0,
//     longitude: 0,
//     address: ''
//   },
//   onLoad() {
//     // 开始实时监听位置变化
//     this.startLocationTracking();
//   },
//   startLocationTracking() {
//     wx.startLocationUpdate({
//       success: () => {
//         console.log('开始实时获取位置');
//         wx.onLocationChange((res) => {
//           console.log('位置变化:', res);
//           // 更新当前位置的经纬度
//           this.setData({
//             latitude: res.latitude,
//             longitude: res.longitude
//           });
//           // 将 GCJ-02 坐标转换为 BD-09 坐标
//           this.convertToBaiduCoordinate(res.latitude, res.longitude);
//         });
//       },
//       fail: (err) => {
//         console.error('实时位置更新失败:', err);
//       }
//     });
//   },
//   // 使用百度地图 SDK 进行坐标转换
//   convertToBaiduCoordinate(latitude, longitude) {
//     const BMap = new bmap.BMapWX({
//       ak: 'b5xnRWivaiEXs9xKP1FwpGeCccvnmnfR' // 百度地图API的Key
//     });
//     BMap.regeocoding({
//       location: `${latitude},${longitude}`,
//       coord_type: 'gcj02', // 指定输入坐标的坐标系
//       success: (res) => {
//         console.log('转换后的百度地图位置信息:', res);
//         const address = res.originalData.result.formatted_address;
//         this.setData({
//           address: address // 更新当前位置的详细地址
//         });
//       },
//       fail: (err) => {
//         console.error('百度地图获取地址失败:', err);
//       }
//     });
//   },
//   /**
//    * 手动将微信经纬度转成百度经纬度方法
//    */
//   gcj02ToBd09(lng, lat) {
//     const x_pi = (Math.PI * 3000.0) / 180.0;
//     const z = Math.sqrt(lng * lng + lat * lat) + 0.00002 * Math.sin(lat * x_pi);
//     const theta = Math.atan2(lat, lng) + 0.000003 * Math.cos(lng * x_pi);
//     const bd_lng = z * Math.cos(theta) + 0.0065;
//     const bd_lat = z * Math.sin(theta) + 0.006;
//     return {
//       lng: bd_lng,
//       lat: bd_lat
//     };
//   },
// });

