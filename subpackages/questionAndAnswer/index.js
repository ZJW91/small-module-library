import Toast from '@vant/weapp/toast/toast';
import {
  queryAllQuestion,
  addSdsUserAnswer
} from "../../api/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    count: 1,
    percentage: 5,
    questionList: [],
    currentAnswer: {}, //当前问题的答案
    answerList: [], // 问题回答列表
    vpDepressiveDisorderId: '',
    openId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options.id)
    this.getAllQuestions()
    // 临时定义
    this.setData({
      openId: wx.getStorageSync('user').openid,
      vpDepressiveDisorderId: options.id
    })
  },
  /**
   * 获取所有题目
   */
  getAllQuestions() {
    queryAllQuestion().then(res => {
      const record = res.result
      record.forEach(item => {
        item.answerList.forEach(i => i.selected = false)
      })
      this.setData({
        questionList: record
      })

    })
  },
  goSelect(e) {
    const key = e.currentTarget.dataset.index
    const currentInfo = e.currentTarget.dataset.item
    const index = this.data.count - 1
    let questionList = this.data.questionList
    questionList[index].answerList.forEach((item, index) => {
      if (key === index) {
        item.selected = true
      } else {
        item.selected = false
      }
    })
    // 问题回答清空
    var currentAnswer = {
      count: this.data.count,
      answerId: currentInfo.id,
      questionId: currentInfo.questionId,
      score: currentInfo.score,
      openId: this.data.openId,
      vpDepressiveDisorderId: this.data.vpDepressiveDisorderId
    }
    this.setData({
      questionList: questionList,
      currentAnswer: currentAnswer
    })
  },
  /**
   * 上一题
   */
  goPre() {
    let index = this.data.count
    if (index === 1) {
      Toast("没有上一题了")
      return
    } else {
      index = index - 1
      this.setData({
        count: index,
        percentage: (index / 20) * 100
      })
    }
  },
  /**
   * 下一题
   */
  goNext() {
    var answerList = this.data.answerList
    const count = this.data.count
    console.log(this.data.currentAnswer)
    let aa = this.data.questionList[this.data.count - 1].answerList
    let isFindSelected = aa.find(item => {
      return item.selected === true;
    });
    if (!isFindSelected) {
      Toast("回答的问题不能为空!")
      return
    }
    if (this.data.currentAnswer.count === count) {
      answerList[this.data.count - 1] = {
        answerId: this.data.currentAnswer.answerId,
        questionId: this.data.currentAnswer.questionId,
        score: this.data.currentAnswer.score,
        openId: 'oQtno4v5wJSog-bfi9MCyH53tUQQ',//写死的
        vpDepressiveDisorderId: this.data.vpDepressiveDisorderId
      }
    }
    if (count === 20) {
      // 提交题目
      addSdsUserAnswer({
        yulongSdsUserAnswerList: answerList
      }).then(res => {
        console.log(res)
        // const score=70
        wx.navigateTo({
          url: './result/index?score='+res.result.sdsScoreRatio+"&info="+res.result.sdsScoreEvaluation+"&num="+res.result.sdsScoreSum,
        })
      })
    } else {
      this.setData({
        answerList: answerList,
        count: count + 1,
        percentage: (count / 20) * 100
        // currentAnswer:{}
      })
    }

  },
  goHome(){
    wx.switchTab({
      url: '/pages/home/index',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})