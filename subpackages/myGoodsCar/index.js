
const app = getApp()
Page({
  data: {
    // 购物车商品
    goodsList: [
      { id: 1, name: '商品名称1', num: 2, price: 100.00, active: false, src: '/images/shopping/good-1.png' },
      { id: 2, name: '商品名称2', num: 1, price: 100.00, active: false, src: '/images/shopping/good-1.png' },
      { id: 3, name: '商品名称3', num: 1, price: 100.00, active: false, src: '/images/shopping/good-1.png' },
      { id: 4, name: '商品名称4', num: 1, price: 100.00, active: false, src: '/images/shopping/good-1.png' },
  
    ],
    activeAll: false,//全选
    totalPrice: 0,
    selectedNum: 0,
    showConfirm: false,
    isEditing: false,
  },


  onLoad: function (options) {
    
  },


  onShow: function () {

    // 如果需要记住用户勾选的记录保存到后台，那么进来就需要查询后台勾选的状态
    // this.calculateSelectedGoods()
    // this.calculatePrice()
  },


  /** 购物车 start */
  hasNoSelected: function () {
    return this.data.goodsList.find(it => {
      return !it.active
    })
  },

  /**
   * 统计勾选的数量
   */
  calculateSelectedGoods: function () {
    const selectedArr = this.data.goodsList.filter(it => {
      return it.active
    })
    this.setData({
      selectedNum: selectedArr.length
    })
  },

  /**
   * 计算商品总价
   */
  calculatePrice: function () {
    let total = 0;
    this.data.goodsList.forEach(it => {
      if (it.active) {
        total += it.price * it.num
      }
    })
    this.setData({
      totalPrice: total
    })
  },

  /**
   * 勾选单个商品
   * @param {*} e 
   */
  changeSelect: function (e) {
    const index = e.detail
    let goodItem = this.data.goodsList[index]
    goodItem.active = !goodItem.active
    if (this.hasNoSelected()) {
      this.setData({
        goodsList: this.data.goodsList,
        activeAll: false,
      })
    } else {
      this.setData({
        goodsList: this.data.goodsList,
        activeAll: true,
      })
    }
    this.calculateSelectedGoods()
    this.calculatePrice()
  },


 
 

  /**
   * 增减商品数量
   * @param {*} e 
   */
  changeNum: function (e) {
    const index = e.detail.index
    const operation = e.detail.operation
    let goodItem = this.data.goodsList[index]
    if (operation === 'up') {
      goodItem.num = ++goodItem.num
    } else {
      goodItem.num = --goodItem.num
    }
    this.setData({
      goodsList: this.data.goodsList
    })
    this.calculateSelectedGoods()
    this.calculatePrice()
  },

  changeSelectAll() {
    const activeAll = this.data.activeAll
    const goodsList = this.data.goodsList.map(item => {
      item.active = !activeAll
      return item
    })
    this.setData({
      activeAll: !activeAll,
      goodsList
    })
    this.calculateSelectedGoods()
    this.calculatePrice()
  },

  changeIsEditing: function(e) {
    const operation = e.currentTarget.dataset.operation
    this.setData({
      isEditing: operation === 'complete' ? false : true
    })
  },

  filterSelected: function() {
    return this.data.goodsList.filter(it => {
      return it.active
    })
  },
  
  toConfirmOrder: function() {
    const goodsList = this.filterSelected()
    console.log('结算',goodsList)
    // wx.navigateTo({
    //   url: './confirmOrder/index?first=true&&item=' + JSON.stringify({ goodsList }),
    // })
  },

  // 删除
  delect_goods:function(){
    const goodsList = this.filterSelected()
    console.log('删除',goodsList)
  },

   /**
   * 删除确认框弹出关闭
   * @param {*} e 
   */
  toDelete: function (e) {
    this.delIndex = e.detail
    this.setData({
      showConfirm: true
    })
  },
  cancel: function() {
    this.setData({
      showConfirm: false
    })
  },
  
  /**
   * 确认删除
   */
  confirmDelete: function() {
    const goodsList = this.data.goodsList
    goodsList.splice(this.delIndex, 1)
    this.setData({
      goodsList: goodsList,
      showConfirm: false
    })
    this.calculateSelectedGoods()
    this.calculatePrice()
  },

  /** 购物车 end */
})
