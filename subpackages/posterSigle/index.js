// pages/poster/poster.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

    showPosterPop:false,//遮罩
    show_bottom_info:false,//底部提示文字
    wxImagePath: '../../images/qrCode.jpg', //二维码图片，一般从后台获取
    posterBg: 'https://covidsh.dkmksh.com/PDF/static/yulong/yl2/saleposter.png',//封面图
    labelList:['工资高','福利好','离家近'],
    canvasImagePath:'',//生成的图片路径
  },


  /**
   * 生成海报
   */
  showExclusiveQRcode() {
    let that = this
    wx.showLoading({
      title: '生成中...',
      mask:true,
    })
    that.setData({
      showPosterPop: true,//打开海报
    })

    const queryDom = wx.createSelectorQuery()//创建了一个选择器查询对象，用于查询页面上的元素。
    queryDom.select('#mycanvas').fields({
        id: true,
        node: true,
        size: true
      }).exec(that.canvasInit.bind(this))//将dom节点传给canvas
  },
  /**
   * 开始绘制各部分
   */
  canvasInit(res) {
    const that = this;
    const canvas = res[0].node
    const ctx = canvas.getContext('2d')
    const dpr = wx.getSystemInfoSync().pixelRatio;//获取屏幕的像素比  值为2
    canvas.width = res[0].width * dpr;
    canvas.height = res[0].height * dpr;
    ctx.scale(dpr, dpr);
    this.setData({
      canvas,
      ctx
    });
  /**
   * 执行顺序
   */
  this.posterBg(ctx, canvas).then(res => {// 绘制背景  
    return this.qrCodeImgCreat(ctx).catch(error=>{// 绘制二维码 
      console.log(error)
      // that.savePoster();  
    })
  }).then(res => {  
    return this.fontDraw(ctx).catch(error=>{// 绘制文字  
      console.log(error)
      // that.savePoster();  
    });  
  }).then(res => {  
    // this.labelDraw(ctx); // 绘制标签  
    return this.headerDraw(ctx).catch(error=>{//绘制头像  
      console.log(error)
      // that.savePoster();  
    }); 
  }).then(res => {  
    that.savePoster(); //生成海报  
  }).catch(error => {  
    console.error(error); // 捕获并打印错误信息  
    that.savePoster(); // 生成海报  
  });
  },
  /**
   * 绘制海报背景
   */
  posterBg(ctx, canvas) {
    return new Promise((resolve,reject) => {
      let img = this.data.canvas.createImage(); //创建img对象
      img.src = this.data.posterBg;
      img.onload = () => {
        this.data.ctx.drawImage(img, 0, 0, 380, 560);
        resolve(true);//回调
      };
      img.onerror=()=>{
        reject('error->>>>> posterBg背景加载失败');  
      }
    })
  },
  /**
   * 绘制二维码
   */
  qrCodeImgCreat(ctx) {
    const that = this;
    return new Promise((resolve,reject) => {
      let img = that.data.canvas.createImage(); //创建img对象
      img.src = that.data.wxImagePath
      img.onload = () => {
        that.data.ctx.drawImage(img, 250, 400, 120, 120);
        resolve(true)
      };
      img.onerror=()=>{
        reject('error->>>>> wxImagePath小程序码加载失败');  
      }

    })
  },
  /**
   * 绘制头像
   */
  headerDraw(ctx) {
    const that = this;
    return new Promise((resolve,reject) => {
      let imgTwo = this.data.canvas.createImage(); //创建img对象
      imgTwo.src = wx.getStorageSync('wxUserInfo').avatarUrl;
      imgTwo.onload = () => {
        ctx.arc(46, 432, 24, 0, 2 * Math.PI)
        ctx.clip()
        ctx.drawImage(imgTwo, 22, 408, 48, 48);//此处位置变动需要同步上面位置
        resolve(true)
      };
      imgTwo.onerror = () => {  
        reject('error->>>>> avatarUrl头像加载失败');  
      };  
    })
  },
  /**
   * 绘制文字
   */
  fontDraw(ctx) {
    let that = this;
    return new Promise((resolve,reject) => {
      try {
        // let text1 = '岗位名';//岗位名
        // ctx.font = 'normal bold 30px sans-serif';
        // ctx.fillStyle = "#fff";
        // ctx.fillText(text1, 24, 60, 200)//ctx.fillText(文字, 像素, 移动y, 移动x)
        // let text2 = '100';//工资金额
        // ctx.fillStyle = "rgba(0,0,0)";
        // ctx.font = 'normal bold 26px sans-serif';
        // ctx.fillStyle = "#fff"; 
        // ctx.fillText(text2, 24, 100, 200);
        // let text3 = "上海";//城市
        // ctx.fillStyle = "rgba(0,0,0)";
        // ctx.font = 'normal 22px sans-serif';
        // ctx.fillStyle = "#fff"; 
        // ctx.fillText(text3, 24, 140, 200);
        // let text4 = "用工单位";//用工单位
        // ctx.fillStyle = "rgba(0,0,0)";
        // ctx.font = 'normal 22px sans-serif';
        // ctx.fillStyle = "#fff"; 
        // ctx.fillText(text4, 24, 180, 200);
        //一下可以判断显示
        // let text = (this.data.userType == '2' || this.data.userType == '3') ? this.data.clientUsersName : this.data.nickName
        let text5 = wx.getStorageSync('wxUserInfo').nickName;
        ctx.font = 'normal  18px sans-serif';
        ctx.fillStyle = "rgba(0, 0, 0, 1)";
        ctx.fillText(text5, 80, 440, 290);
        let text6 = '推荐给你一个好岗位';
        ctx.font = 'normal  14px sans-serif';
        ctx.fillStyle = "rgba(0, 0, 0, 0.7)";
        ctx.fillText(text6, 24, 480, 310);
        let text7 = '长按识别查看详情';
        ctx.fillStyle = "rgba(0,0,0)";//添加颜色
        ctx.font = 'normal  12px sans-serif';//指定文字样式
        ctx.fillStyle = "rgba(0, 0, 0, 0.4)"; //新增样式
        ctx.fillText(text7, 24, 510, 280);
        resolve(true)
      } catch (error) {
        reject("错误->>>绘制文字出错")
      }
    })
  },
  /**
   * 生成图片
   */
  savePoster() {
    var that = this;
    wx.showLoading({
      title: '生成中...',
      mask:true,
    })
    // console.log('保存canvasId', this.data.canvas._canvasId)
    wx.canvasToTempFilePath({     //将canvas生成图片
      canvas: this.data.canvas,
      x: 0,
      y: 0,
      width: 380,
      height: 562,
      destWidth: 380 * 2,     //截取canvas的宽度
      destHeight: 562 * 2,   //截取canvas的高度
      success: function (res) {
        // console.log('生成图片成功1：', res)
        that.setData({
          canvasImagePath:res.tempFilePath,
          show_bottom_info:true,
        })
        wx.hideLoading();
      },
    }, this)
  },
  //关闭海报弹框
  onClickposterHide() {
    this.setData({
      showPosterPop: false,//关闭遮罩
      canvasImagePath:'',//清空上一次的路径
      show_bottom_info:false,//关闭底部提示
    })
  },
 /**
   * 绘制图形标签
   */
  labelDraw(ctx) {
    var that = this;
    for(var i = 0; i < that.data.labelList.length; i++){
    let text = that.data.labelList[i];
     //画圆角边框
     ctx.font = 'normal  14px sans-serif';
     ctx.fillStyle = "#fff";
     ctx.textAlign='left';
     var discountText = text
     var bdColor = '#ededed';
     var bdBackground = 'transparent';
     var bdRadius = 4;
     var textPadding = 6;
     var boxHeight = 20;
     console.log(ctx.measureText(discountText).width);//每个标签的宽度
     var boxWidth,boxWidth1,boxWidth2;
     //标签之间等距
     if(i == 0){
       boxWidth = ctx.measureText(discountText).width + textPadding * 2;
      ctx.fillText(discountText, 30, 220);
      that.roundRect(ctx, 25, 205, boxWidth, boxHeight, bdRadius, bdBackground, bdColor)
     }else if(i == 1){
      boxWidth1 = ctx.measureText(discountText).width + textPadding * 2;
      ctx.fillText(discountText, 30+boxWidth+5, 220);
      that.roundRect(ctx, 25+boxWidth+5, 205, boxWidth1, boxHeight, bdRadius, bdBackground, bdColor)
     }else if(i == 2){
      boxWidth2 = ctx.measureText(discountText).width + textPadding * 2;
      ctx.fillText(discountText, 30+boxWidth+boxWidth1+10, 220);
      that.roundRect(ctx, 25+boxWidth+boxWidth1+10, 205, boxWidth2, boxHeight, bdRadius, bdBackground, bdColor)
     }
    }
  },
  //标签画圆角边框
  roundRect(ctx, x, y, w, h, r, fillColor, strokeColor) {
    // 画圆角 ctx、x起点、y起点、w宽度、y高度、r圆角半径、fillColor填充颜色、strokeColor边框颜色
    // 开始绘制
    ctx.beginPath()
    // 绘制左上角圆弧 Math.PI = 180度
    // 圆心x起点、圆心y起点、半径、以3点钟方向顺时针旋转后确认的起始弧度、以3点钟方向顺时针旋转后确认的终止弧度
    ctx.arc(x + r, y + r, r, Math.PI, Math.PI * 1.5)
    // 绘制border-top
    // 移动起点位置 x终点、y终点
    ctx.moveTo(x + r, y)
    // 画一条线 x终点、y终点
    ctx.lineTo(x + w - r, y)
    // ctx.lineTo(x + w, y + r)
    // 绘制右上角圆弧
    ctx.arc(x + w - r, y + r, r, Math.PI * 1.5, Math.PI * 2)
    // 绘制border-right
    ctx.lineTo(x + w, y + h - r)
    // ctx.lineTo(x + w - r, y + h)
    // 绘制右下角圆弧
    ctx.arc(x + w - r, y + h - r, r, 0, Math.PI * 0.5)
    // 绘制border-bottom
    ctx.lineTo(x + r, y + h)
    // ctx.lineTo(x, y + h - r)
    // 绘制左下角圆弧
    ctx.arc(x + r, y + h - r, r, Math.PI * 0.5, Math.PI)
    // 绘制border-left
    ctx.lineTo(x, y + r)
    // ctx.lineTo(x + r, y)
    if (fillColor) {
      // 因为边缘描边存在锯齿，最好指定使用 transparent 填充
      ctx.fillStyle = fillColor;
      // 对绘画区域填充
      ctx.fill()
    }
    if (strokeColor) {
      // 因为边缘描边存在锯齿，最好指定使用 transparent 填充
      ctx.strokeStyle=strokeColor;
      // 画出当前路径的边框
      ctx.stroke()
    }
    // 关闭一个路径
    // ctx.closePath()
    // 剪切，剪切之后的绘画绘制剪切区域内进行，需要save与restore
    // ctx.clip()
  },

  
})