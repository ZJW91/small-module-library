// subpackages/pdf/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },


    /**
   * 下载查看pdf
   * @param url 
   */
  showPdf(url){
    console.log("showPdf",url)
    const that = this;
    let pdfurl = "http://wx.yulongsh.com/wxreport/report/v1/pdfShow?pipe_code=910242576242&sex=2&idCard=210102193312065649&patientName=%E5%BE%90%E5%8A%9B%E4%BB%81&phone=15618967613"
    wx.downloadFile({
      url:pdfurl, 
      success: (res)=>{
        wx.hideLoading()
        let filePath = res.tempFilePath;
        if(res.statusCode === 200){
          wx.getFileSystemManager().getFileInfo({
            filePath:filePath,
            success: info=>{
              console.log("====filesize = "+info.size);
                wx.openDocument({
                  filePath: filePath,
                  showMenu: true,
                  fileType: 'pdf',
                  success: function (res) {}
                }); 
            }
          })
        }else{
         console.log("error")
        }
        console.log(res)
      }
    })
  },

  /**
   * 两种方式加载pdf
   * 1、使用web-view组件，需要考虑pdf链接域名配置业务域名的问题，然后ios可以预览，安卓没有预览pdf的浏览器
   * 2、使用wx.downloadFile和wx.openDocument方法，兼容性比较好，也可以分享保存
   */

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})