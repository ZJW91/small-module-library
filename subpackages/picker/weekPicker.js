// 引入 weekUtils.js
const weekUtils = require('../..//utils/weekUtils.js');
Page({
  data: {
    columns: weekUtils.getWeekList(), // 用于存储周区间
    weekValue: '',
  },

  onLoad() {
  
  },

  onWeekChange(e) {
    const index = e.detail.value;
    const selectedWeek = this.data.columns[index];
    const result = weekUtils.parseWeekString(selectedWeek);
    console.log(`周数: ${result.weekNumber}`); // 输出周数
    console.log(`起始日期: ${result.startDate}`); // 输出起始日期
    console.log(`结束日期: ${result.endDate}`); // 输出结束日期
    this.setData({ weekValue:selectedWeek });
  }
});
