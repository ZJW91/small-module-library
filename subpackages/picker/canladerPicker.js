// subpackages/picker/canladerPicker.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultDate: [], // 默认选中的日期区间，初始化为空数组
    minDate: new Date(2020, 0, 1).getTime(),
    maxDate: new Date(2030, 0, 31).getTime(),
    pickerDateValue: '',
    datePickerShow: false,
    startDateStr:'',// 选择的日期
    endDateStr:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

    /**
   * 日历空间
   */
  // 打开
  onDisplay() {
    this.setData({ datePickerShow: true });
  },
  // 关闭
  onClose() {
    this.setData({ datePickerShow: false });
  },
  // 格式化
  formatDate(date) {
    date = new Date(date);
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  },
  onConfirm(event) {
    const [start, end] = event.detail;
    this.setData({
      datePickerShow: false,
      pickerDateValue: `${this.formatDate(start)} ~ ${this.formatDate(end)}`,
      startDateStr:`${this.formatDate(start)}`,
      endDateStr:`${this.formatDate(end)}`,
    });
  },

 /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    let defaultDate = JSON.parse(JSON.stringify(this.currentMonthFirstAndFifteenth()))
    let startDateDefault = this.formatDate(defaultDate[0])
    let endDateDefault = this.formatDate(defaultDate[1])
    this.setData({
      defaultDate:defaultDate,
      startDateStr:startDateDefault,
      endDateStr:endDateDefault,
      pickerDateValue:startDateDefault + '~' + endDateDefault
    })
  },

  /**
   * 设置默认1号和15号
   */
  currentMonthFirstAndFifteenth() {
    const now = new Date();
    const year = now.getFullYear();
    const month = now.getMonth();
    return [
      new Date(year, month, 1), // 当月1号
      new Date(year, month, 15), // 当月15号
    ];
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})