
const app = getApp()

//参考：https://blog.csdn.net/qq_39951524/article/details/132340157

function inArray(arr, key, val) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i][key] === val) {
      return i;
    }
  }
  return -1;
}

// ArrayBuffer转16进度字符串示例
function ab2hex(buffer) {
  var hexArr = Array.prototype.map.call(
    new Uint8Array(buffer),
    function (bit) {
      return ('00' + bit.toString(16)).slice(-2)
    }
  )
  return hexArr.join('');
}

Page({
  data: {
    devices: [],
    connected: false,
    chs: [],
  },
  getCeshi() {
    wx.navigateTo({
      url: '/pages/main/main',
    })
  },
  // 搜寻周边蓝牙
  openBluetoothAdapter() {
    //console.log('openBluetoothAdapter success')
    wx.openBluetoothAdapter({
      success: (res) => {
        console.log('openBluetoothAdapter success', res)
        this.startBluetoothDevicesDiscovery()
      },
      fail: (res) => {
        if (res.errCode === 10001) {
          wx.onBluetoothAdapterStateChange(function (res) {
            console.log('onBluetoothAdapterStateChange', res)
            if (res.available) {
              this.startBluetoothDevicesDiscovery()
            }
          })
        }
      }
    })
  },
  // 停止搜寻周边蓝牙
  getBluetoothAdapterState() {
    wx.getBluetoothAdapterState({
      success: (res) => {
        console.log('getBluetoothAdapterState', res)
        if (res.discovering) {
          this.onBluetoothDeviceFound()
        } else if (res.available) {
          this.startBluetoothDevicesDiscovery()
        }
      }
    })
  },
  // 开始搜寻附近的蓝牙外围设备
  startBluetoothDevicesDiscovery() {
    if (this._discoveryStarted) {
      return
    }
    this._discoveryStarted = true
    wx.startBluetoothDevicesDiscovery({
      allowDuplicatesKey: true,
      success: (res) => {
        console.log('startBluetoothDevicesDiscovery success', res)
        this.onBluetoothDeviceFound()
      },
    })
  },
  //停止搜寻附近的蓝牙外围设备。若已经找到需要的蓝牙设备并不需要继续搜索时，建议调用该接口停止蓝牙搜索。
  stopBluetoothDevicesDiscovery() {
    wx.stopBluetoothDevicesDiscovery()
  },
  //监听搜索到新设备的事件
  onBluetoothDeviceFound() {
    wx.onBluetoothDeviceFound((res) => {
      res.devices.forEach(device => {
        if (!device.name && !device.localName) {
          return
        }
        const foundDevices = this.data.devices
        const idx = inArray(foundDevices, 'deviceId', device.deviceId)
        const data = {}
        if (idx === -1) {
          data[`devices[${foundDevices.length}]`] = device
        } else {
          data[`devices[${idx}]`] = device
        }
        this.setData(data)
      })
    })
  },
  //连接蓝牙低功耗设备。
  createBLEConnection(e) {
    const ds = e.currentTarget.dataset
    const deviceId = ds.deviceId
    const name = ds.name
    wx.createBLEConnection({
      deviceId,
      success: (res) => {
        this.setData({
          connected: true,
          name,
          deviceId,
        })
        this.getBLEDeviceServices(deviceId)
      }
    })
    this.stopBluetoothDevicesDiscovery()
  },
  closeBLEConnection() {
    wx.closeBLEConnection({
      deviceId: this.data.deviceId
    })
    this.setData({
      connected: false,
      chs: [],
      canWrite: false,
    })
  },
  //获取蓝牙低功耗设备所有服务。
  getBLEDeviceServices(deviceId) {
    console.log('deviceId ========', deviceId)
    wx.getBLEDeviceServices({
      deviceId,
      success: (res) => {
        for (let i = 0; i < res.services.length; i++) {
          //获取通过设备id多个特性服务serviceid （包含 读、写、通知、等特性服务）
         //   console.log('serviceId ========', res.services[2].uuid)
          
         // 通过上边注释解封，排查到判断服务id 中第三个服务id  代表写入服务
        //  isPrimary代表 ：判断服务id是否为主服务
         // if (res.services[2].isPrimary) {
         //  this.getBLEDeviceCharacteristics(deviceId, res.services[2].uuid)
        //  }
           //判断通过（设备id获取的多个特性服务）中是否有与（蓝牙助手获取的写入特性服务），相一致的serviceid
          if (res.services[i].uuid=="0000FFE0-0000-1000-8000-00805F9B34FB") {
            this.getBLEDeviceCharacteristics(deviceId, res.services[i].uuid) 
          }
        }
      }
    })
  },
  //获取蓝牙低功耗设备某个服务中所有特征 (characteristic)。
  getBLEDeviceCharacteristics(deviceId, serviceId) {
    console.info("蓝牙的deviceId====" + deviceId);
    console.info("蓝牙服务的serviceId====" + serviceId);
    wx.getBLEDeviceCharacteristics({
      deviceId,
      serviceId,
      success: (res) => {
        console.log('getBLEDeviceCharacteristics success', res.characteristics)
        for (let i = 0; i < res.characteristics.length; i++) {
          let item = res.characteristics[i]
          if (item.properties.read) {
            wx.readBLECharacteristicValue({
              deviceId,
              serviceId,
              characteristicId: item.uuid,
            })
          }
          if (item.properties.write) {
            this.setData({
              canWrite: true
            })
            this._deviceId = deviceId
            this._serviceId = serviceId
            this._characteristicId = item.uuid

            console.info("写入（第一步）的characteristicId====" + item.uuid);
            //初始化调用 写入信息的方法  1：代表开灯
            this.writeBLECharacteristicValue("1")
          }
          if (item.properties.notify || item.properties.indicate) {
            wx.notifyBLECharacteristicValueChange({
              deviceId,
              serviceId,
              characteristicId: item.uuid,
              state: true,
            })
          }
        }
      },
      fail(res) {
        console.error('getBLEDeviceCharacteristics', res)
      }
    })
    // 操作之前先监听，保证第一时间获取数据
    wx.onBLECharacteristicValueChange((characteristic) => {
      const idx = inArray(this.data.chs, 'uuid', characteristic.characteristicId)
      const data = {}
      if (idx === -1) {
        data[`chs[${this.data.chs.length}]`] = {
          uuid: characteristic.characteristicId,
          value: ab2hex(characteristic.value)
        }
      } else {
        data[`chs[${idx}]`] = {
          uuid: characteristic.characteristicId,
          value: ab2hex(characteristic.value)
        }
      }
     
      this.setData(data)
    })
  },
  
/**
 * 写入的数据 格式转换
 * @param str
 * @returns 字符串 转 ArrayBuffer
 */
hex2buffer(str) {
  console.info("写入的数据====" + str);
  //字符串 转 十六进制
  var val = "";
  for (var i = 0; i < str.length; i++) {
      if (val == "")
          val = str.charCodeAt(i).toString(16);
      else
          val += "," + str.charCodeAt(i).toString(16);
  }
  //十六进制 转 ArrayBuffer
  var buffer = new Uint8Array(val.match(/[\da-f]{2}/gi).map(function (h) {
      return parseInt(h, 16)
  })).buffer;

  return buffer;
},
//开灯
opendeng() {
  
  // 调用写入信息的方法 向蓝牙设备发送一个开灯的参数数据 ,  1：代表开灯
  var writeValue ="1";
  this.writeBLECharacteristicValue(writeValue)
 
},
//关灯
guandeng() {

    // 调用写入信息的方法 向蓝牙设备发送一个关灯的参数数据 ,  0：代表关灯
     var writeValue ="0";
     this.writeBLECharacteristicValue(writeValue)
    
},
writeBLECharacteristicValue(writeValue) {
  // 向蓝牙设备发送一个0x00的16进制数据
 // let buffer = new ArrayBuffer(1)
 // let dataView = new DataView(buffer)
 //  dataView.setUint8(0, 0)
 //调用hex2buffer（）方法，转化成ArrayBuffer格式
 console.log('获取传递参数writeValue的数据为=====',writeValue)
 var buffer =this.hex2buffer(writeValue);
  console.log('获取二进制数据',buffer)
  //向低功耗蓝牙设备特征值中写入二进制数据。
  wx.writeBLECharacteristicValue({
    deviceId: this._deviceId,
    serviceId: this._serviceId,
    characteristicId: this._characteristicId,
    value: buffer,
    success (res) {
      console.log('成功写数据writeBLECharacteristicValue success', res)
      //如果 uni.writeBLECharacteristicValue 走 success ，证明你已经把数据向外成功发送了，但不代表设备一定就收到了。通常设备收到你发送过去的信息，会返回一条消息给你，而这个回调消息会在 uni.onBLECharacteristicValueChange 触发
    },
     fail(res) {
      console.error('失败写数据getBLEDeviceCharacteristics', res)
    }
  })
},
//断开与蓝牙低功耗设备的连接。
  closeBluetoothAdapter() {
    wx.closeBluetoothAdapter()
    this._discoveryStarted = false
  },
})



