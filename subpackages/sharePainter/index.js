const app = getApp()

Page({
  data: {
    nickName: '',
    avatarUrl: '',
    isCanDraw: false
  },
  onLoad() {
    this.setData({
      nickName: wx.getStorageSync('nickName') || '',
      avatarUrl: wx.getStorageSync('avatarUrl') || ''
    })
  },
  handleClose() {
    this.setData({
      isCanDraw: !this.data.isCanDraw
    })
  },


  // 获取用户信息。页面产生点击事件（例如 button 上 bindtap 的回调中）后才可调用，
  // 每次请求都会弹出授权窗口，用户同意后返回 userInfo。该接口用于替换 wx.getUserInfo
  onGotUserInfo: function (e) {
    //防止重复多次点击，此处需要节流或者增加loading
    wx.getUserProfile({
      desc: "获取你的昵称、头像、地区及性别",
      success: res => {
        console.log(res)
        let wxUserInfo = res.userInfo;
        // console.log(wxUserInfo);
        // console.log("nickName=" + wxUserInfo.nickName);
        // console.log("avatarUrl=" + wxUserInfo.avatarUrl);
        // console.log(JSON.stringify(wxUserInfo));

        wx.setStorageSync('avatarUrl', wxUserInfo.avatarUrl);
        wx.setStorageSync('nickName', wxUserInfo.nickName);
        this.setData({
          nickName: wxUserInfo.nickName,
          avatarUrl: wxUserInfo.avatarUrl,
          isCanDraw: !this.data.isCanDraw
        })
  
      },
      fail: res => {
         //拒绝授权
         wx.showToast({
          title: '您拒绝了授权',
          icon: 'none',
          duration: 1500
        })
        return;
      }
    })
  },

})
