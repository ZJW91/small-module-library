import {request} from './request'
module.exports = {
  queryAllQuestion:(data) => request('/yulongSdsUserAnswer/queryAllQuestion', "GET", data,false),
  addSdsUserAnswer:(data) => request('/yulongSdsUserAnswer/addSdsUserAnswer', 'POST', data,true),
  consultation:(data) => request('/consultation/list', 'GET', data,true),
}

