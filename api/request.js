const config = require("../api/config.js")
const baseUrl = config.baseUrl
module.exports = {
  request: function (url, method, data, isJson) {
    let header = {
      timeOffset: 60000,
      'content-type': 'application/x-www-form-urlencoded'
    }
    let time = new Date().getTime()
    url += '?time=' + time + '&'
    if (isJson) {
      header = {
        timeOffset: 60000,
        "content-type": 'application/json'
      }
    }
    return new Promise(function (resolve, reject) {
      wx.request({
        url: baseUrl + url,
        method: method,
        data: data,
        header: header,
        success(res) {
          if (res.data.code === 200 || res.statusCode === 200) {
            if (res.data.code == 400) {
              wx.showToast({
                title: res.data.msg,
                icon: "none"
              })
            } else {
              resolve(res.data);
            }
          } else if (res.data.code === 10002) {
            //token过期
            wx.showModal({
              title: '提示',
              content: '身份信息失效！请重新登录！',
              showCancel: false,
              success(res) {
                if (res.confirm) {
                  wx.navigateTo({//重新去登录页
                    url: '/subpkg/login/login'
                  })
                } else if (res.cancel) {
                  wx.setStorageSync("token", "")
                }
              }
            })
          } else {
            // console.log("111111")
            wx.showToast({
              title: res.data.message,
              icon: "none"
            })
            reject(res.data);
          }
        },
        fail(err) {
          wx.showToast({
            title: "无法连接到服务器",
            icon: "none"
          })
          reject(err)
        }
      })
    })
  }

}