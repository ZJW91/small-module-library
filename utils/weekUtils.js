// 获取周区间数据的方法
function getWeekList() {
  const oneDayTime = 24 * 60 * 60 * 1000; // 一天的毫秒数
  let currentMonday = getMonday(new Date()); // 获取当前周一的日期
  let weekList = [];

  for (let i = 0; i < 365; i += 7) {
    let weekNumber = getWeekNumber(currentMonday); // 获取第几周
    let sunday = new Date(currentMonday.getTime() + 6 * oneDayTime); // 计算当前周日
    let weekText = `第${weekNumber}周 ${formatDate(currentMonday)}-${formatDate(sunday)}`;
    weekList.push(weekText);
    // 往前推一周
    currentMonday.setDate(currentMonday.getDate() - 7);
  }

  return weekList;
}

// 获取某日期当周的周一
function getMonday(date) {
  let day = date.getDay() || 7; // 周日返回 7
  if (day !== 1) date.setHours(-24 * (day - 1)); // 设置为当前周一
  return new Date(date.setHours(0, 0, 0, 0)); // 只保留日期部分
}

// 计算一年中的第几周
function getWeekNumber(date) {
  const startOfYear = new Date(date.getFullYear(), 0, 1); // 当年的1月1日
  const dayOfYear = ((date - startOfYear) / (24 * 60 * 60 * 1000)) + 1; // 第几天
  return Math.ceil(dayOfYear / 7); // 返回周数
}

// 格式化日期为 YYYY年MM月DD日
function formatDate(date) {
  return `${date.getFullYear()}年${(date.getMonth() + 1).toString().padStart(2, '0')}月${date.getDate().toString().padStart(2, '0')}日`;
}

// 解析周区间字符串
function parseWeekString(weekStr) {
  const regex = /第(\d+)周\s(\d{4}年\d{2}月\d{2}日)-(\d{4}年\d{2}月\d{2}日)/;
  const match = weekStr.match(regex);
  if (match) {
    return {
      weekNumber: match[1],
      startDate: formatDateToDash(match[2]),
      endDate: formatDateToDash(match[3])
    };
  }
  return null;
}

// 将 YYYY年MM月DD日 转换为 YYYY-MM-DD
function formatDateToDash(dateStr) {
  return dateStr.replace(/年|月/g, '-').replace('日', '');
}

module.exports = {
  getWeekList,
  parseWeekString
};
