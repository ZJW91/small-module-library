// components/tabList/tabList.js
Component({
  options: {
      // multipleSlots: true // 启用插槽
  },
  /**
   * 组件的属性列表
   */
  properties: {
    tabParams:{
      type: Object,
      value:{

      }
    },
    tabList:{
      type: Array,
      value:[]
    },
  },
 
  /**
   * 组件的初始数据
   */
  data: {

  },
 
  /**
   * 组件的方法列表
   */
  methods: {
  // 点击导航切换
  changeTab(e){
    // console.log(e)
    let type = e.currentTarget.dataset.type;//当前点击导航级别  1 代表一级
    let id = e.currentTarget.dataset.id;//当前点击tab的id
    let listdata = e.currentTarget.dataset.listdata;//点击当前list
    let sort = e.currentTarget.dataset.sort;//当前点击导航id
    let index = e.currentTarget.dataset.index;//当前点击导航 index
    
    if(type==1){//一级菜单
      this.setData({
        'tabParams.chooseCurrentParentIndex':index,
        'tabParams.chooseCurrentSecondIndex':0,
        'tabParams.chooseCurrentTabFirst':sort,
        'tabParams.chooseCurrentTabSecond':this.data.tabList[index].childList[0].sort,
        'tabParams.tabType':1,
        // 'tabParams.tabName':listdata.title,
        // 'tabParams.tabId':listdata.childList[0].id//因为一级菜单点击默认选中第一个二级菜单
      })
    }else{
      this.setData({
        'tabParams.chooseCurrentTabSecond':sort,
        'tabParams.chooseCurrentSecondIndex':index,
        'tabParams.tabType':2,
        // 'tabParams.tabId':id
      })
    };
    this.triggerEvent('changeTab',this.data.tabParams)
  },
  },
  lifetimes:{
    attached(){
     
    }
  }
})