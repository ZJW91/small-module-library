// components/UserInfoGet/child/index.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    value: Boolean,
  },

  /**
   * 组件的初始数据
   */
  data: {
    headimg:'../../images/l-unhead.png',
    avatarUrl:'',
    nickName:'',
    openid:'',
  },

  /**
   * 组件的方法列表
   */
  methods: {
    /**
     * 填写昵称
     * @param {*} e 
     */
    getNickname(e){
      let that = this;
      that.setData({
        nickName:e.detail.value
      })
    },
    /**
     * 点击头像输入框
     * @param {*} e 
     */
    onChooseAvatar(e){
      let that = this;
      that.uploadImg(e.detail.avatarUrl)
    },
    async uploadImg(url) {
      let that = this;
      that.setData({
        avatarUrl:url,
      })
  },



    /**
     * 授权
     */
    async authorize(){
      let that = this;
      if(!that.data.avatarUrl){
          wx.showToast({
              title:'请选择头像',
              icon:"none",
              duration:1500
          })
          return
      }
      that.setData({
        nickName:that.data.nickName.trim()//去掉空格
      })
      if(!that.data.nickName){
          wx.showToast({
              title:'请填写昵称',
              icon:"none",
              duration:1500
          })
          return
      }
   
      let userObj = {}
      userObj.avatarUrl = this.data.avatarUrl
      userObj.nickName = this.data.nickName
      wx.setStorageSync('wechatUserInfo', JSON.stringify(userObj))//存储本地
      this.triggerEvent('getWechatUserInfo', userObj)
      this.triggerEvent('closePop', false)//关闭弹框
  },

  
    /**
     * 取消
     */
    closeStudent() {
      let that = this;
      that.setData({
        avatarUrl:'',
        nickName:'',
      })
      that.triggerEvent('closePop',false)//关闭弹框
    },
  }
})